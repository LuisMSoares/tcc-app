import customHeader from '../customHeader';
import api from '../../../../services/api';

import React, {Component} from 'react';
import { CalendarList } from 'react-native-calendars';
import {View, StatusBar} from 'react-native';

class detailsAbsence extends Component{
  static navigationOptions = {
    header: null,
  };

  state = {
    dMarks: {},
  };
  
  componentDidMount(){
    this._getDates();
  }

  _getDates = async () => {
    let presence = await this.props.navigation.getParam('presence', -1);
    const subjId = await this.props.navigation.getParam('subjId', 0);
    const userId = await this.props.navigation.getParam('userId', 0);
    if (presence === -1){
      const datesResult = await api.get(`/absence/dates/${subjId}`);
      presence = datesResult.data.dates;
    }

    const response = await api.get(`/absence/one/subjectId=${subjId}&userid=${userId}`);
    if (response.status === 200){
      const my_presence = response.data.dates;

      let dateMarks = {}
      for (let date of presence) {
        if(my_presence.includes(date)){
          dateMarks[date] = {disabled: true, startingDay: true, endingDay: true, color: '#a7e0a3', textColor: 'black'};
        }else{
          dateMarks[date] = {disabled: true, startingDay: true, endingDay: true, color: '#ff6746', textColor: 'white'};
        }
      }
      this.setState({dMarks: dateMarks})
    }
  };

  render(){
    return(
      <View style={{ flex: 0, }}>
        {customHeader(this.props.navigation.toggleDrawer,"Calendario de Faltas")}
        <CalendarList
          // Callback which gets executed when visible months change in scroll view. Default = undefined
          onVisibleMonthsChange={(months) => {}}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={50}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={50}
          // Enable or disable scrolling of calendar list
          scrollEnabled={true}
          // Enable or disable vertical scroll indicator. Default = false
          showScrollIndicator={true}
          theme={{arrowColor: 'white'}}
          markedDates={this.state.dMarks}
          markingType={'period'}
        />
      </View>
    );
  }
}

export default detailsAbsence;